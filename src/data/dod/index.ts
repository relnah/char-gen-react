import { RaceType } from "../../components/dod/Race";
import { ClassType } from "../../components/dod/Class";

const attrCostObject = require("../../data/dod/attr-cost.json");
let attrCost: Map<string, number> = new Map<string, number>();

for (const key in attrCostObject) {
  if (Object.prototype.hasOwnProperty.call(attrCostObject, key)) {
    const element = attrCostObject[key];
    attrCost.set(key, element);
  }
}

const sizeNegCostObject = require("../../data/dod/siz-neg-cost.json");
let sizeNegCost: Map<string, number> = new Map<string, number>();

for (const key in sizeNegCostObject) {
  if (Object.prototype.hasOwnProperty.call(sizeNegCostObject, key)) {
    const element = sizeNegCostObject[key];
    sizeNegCost.set(key, element);
  }
}

const races: RaceType[] = require("../../data/dod/races.json");
const classes: ClassType[] = require("../../data/dod/classes.json");

export { races, classes, attrCost, sizeNegCost };
