import { Outlet, Link } from "react-router-dom";

function DefaultLayout() {
  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/">Start</Link>
          </li>
          <li>
            <Link to="/dod">Drakar & Demoner</Link>
          </li>
          <li>
            <Link to="/dnd">Dungeons and Dragons</Link>
          </li>
        </ul>
      </nav>

      <Outlet />
    </>
  );
}

export default DefaultLayout;
