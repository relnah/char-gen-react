import { useCallback, useState } from "react";
import Race, { RaceType } from "../../components/dnd/Race";

function Dnd() {
  const [race, setRace] = useState<RaceType>({ name: "", str: 0 });

  const updateRace = useCallback((race: RaceType) => {
    setRace(race);
  }, []);

  return (
    <>
      <h1>Dungeons and Dragons</h1>
      <p>Race: {race.name}</p>
      <Race race={race} changeRace={updateRace} />
    </>
  );
}

export default Dnd;
