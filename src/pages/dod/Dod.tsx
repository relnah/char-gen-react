import { useCallback, useState } from "react";
import Attribute from "../../components/dod/Attribute";
import Class, { ClassType } from "../../components/dod/Class";
import Race, { RaceType } from "../../components/dod/Race";
import { attrCost, sizeNegCost } from "../../data/dod";

type Character = {
  name: string;
  race: string;
  class: string;
  str: CharacterAttribute;
  dex: CharacterAttribute;
  con: CharacterAttribute;
  int: CharacterAttribute;
  wis: CharacterAttribute;
  cha: CharacterAttribute;
  siz: CharacterSize;
  hit: number;
};

type CharacterAttribute = {
  value: number;
  mod: number;
  req: number;
  cost: number;
};

type CharacterSize = {
  value: number;
  normal: number;
  min: number;
  max: number;
  cost: number;
};

const blankCharacter: Character = {
  name: "Blank Character",
  race: "",
  class: "",
  str: { value: 0, mod: 0, req: 0, cost: 0 },
  dex: { value: 0, mod: 0, req: 0, cost: 0 },
  con: { value: 0, mod: 0, req: 0, cost: 0 },
  int: { value: 0, mod: 0, req: 0, cost: 0 },
  wis: { value: 0, mod: 0, req: 0, cost: 0 },
  cha: { value: 0, mod: 0, req: 0, cost: 0 },
  siz: { value: 0, normal: 0, min: 0, max: 0, cost: 0 },
  hit: 0,
};

export type AttributeValue = {
  attr: string;
  value: number;
};

function sizeCost(sizeValue: number, sizeNormal: number): number {
  if (sizeValue > sizeNormal) {
    return (sizeValue - sizeNormal) * 2;
  } else if (sizeValue < sizeNormal) {
    return sizeNegCost.get((sizeNormal - sizeValue).toString()) ?? 0;
  } else {
    return 0;
  }
}

function hitPoints(character: Character): number {
  const con = character.con.value + (character.con.mod ?? 0);
  const size = character.siz.value;
  return Math.ceil((con + size) / 2);
}

function totalCost(character: Character): number {
  return (
    character.str.cost +
    character.dex.cost +
    character.con.cost +
    character.int.cost +
    character.wis.cost +
    character.cha.cost +
    character.siz.cost
  );
}

function Dod() {
  const [character, setCharacter] = useState<Character>(blankCharacter);

  const setAttribute = useCallback(
    (attribValue: AttributeValue) => {
      setCharacter((previousState) => {
        let newValue: CharacterAttribute;
        let newSizeValue: CharacterSize;
        console.log("setAttrib");
        switch (attribValue.attr) {
          case "str":
            newValue = {
              ...previousState["str"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, str: newValue };
          case "dex":
            newValue = {
              ...previousState["dex"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, dex: newValue };
          case "con":
            newValue = {
              ...previousState["con"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, con: newValue };
          case "int":
            newValue = {
              ...previousState["int"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, int: newValue };
          case "wis":
            newValue = {
              ...previousState["wis"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, wis: newValue };
          case "cha":
            newValue = {
              ...previousState["cha"],
              value: attribValue.value ?? 0,
              cost: attrCost.get(attribValue.value.toString()) ?? 0,
            };
            return { ...previousState, cha: newValue };
          case "siz":
            newSizeValue = {
              ...previousState["siz"],
              value: attribValue.value ?? 0,
              cost: sizeCost(attribValue.value, character.siz.normal),
            };
            return { ...previousState, siz: newSizeValue };

          default:
            return previousState;
        }
      });
    },
    [character.siz.normal]
  );

  const selectRace = useCallback((race: RaceType) => {
    setCharacter((previousState) => {
      const newStr: CharacterAttribute = {
        ...previousState.str,
        mod: race.str ?? 0,
      };
      const newDex: CharacterAttribute = {
        ...previousState.dex,
        mod: race.dex ?? 0,
      };
      const newCon: CharacterAttribute = {
        ...previousState.con,
        mod: race.con ?? 0,
      };
      const newInt: CharacterAttribute = {
        ...previousState.int,
        mod: race.int ?? 0,
      };
      const newWis: CharacterAttribute = {
        ...previousState.wis,
        mod: race.wis ?? 0,
      };
      const newCha: CharacterAttribute = {
        ...previousState.cha,
        mod: race.cha ?? 0,
      };
      const newSiz: CharacterSize = {
        ...previousState.siz,
        normal: race.siz.normal,
        value: race.siz.normal,
        max: race.siz.max,
        min: race.siz.min,
      };
      return {
        ...previousState,
        race: race.name,
        str: newStr,
        dex: newDex,
        con: newCon,
        int: newInt,
        wis: newWis,
        cha: newCha,
        siz: newSiz,
      };
    });
  }, []);

  const selectClass = useCallback((characterClass: ClassType) => {
    setCharacter((previousState) => {
      const newStr: CharacterAttribute = {
        ...previousState.str,
        req: characterClass.str ?? 0,
      };
      const newDex: CharacterAttribute = {
        ...previousState.dex,
        req: characterClass.dex ?? 0,
      };
      const newCon: CharacterAttribute = {
        ...previousState.con,
        req: characterClass.con ?? 0,
      };
      const newInt: CharacterAttribute = {
        ...previousState.int,
        req: characterClass.int ?? 0,
      };
      const newWis: CharacterAttribute = {
        ...previousState.wis,
        req: characterClass.wis ?? 0,
      };
      const newCha: CharacterAttribute = {
        ...previousState.cha,
        req: characterClass.cha ?? 0,
      };
      return {
        ...previousState,
        class: characterClass.name,
        str: newStr,
        dex: newDex,
        con: newCon,
        int: newInt,
        wis: newWis,
        cha: newCha,
      };
    });
  }, []);

  return (
    <>
      <h1>Drakar & Demoner</h1>
      <p>
        {character.race} - {character.class}
      </p>
      <p>
        HP: {hitPoints(character)}
        <br />
        BP kostnad: {totalCost(character)}
      </p>
      <p>
        <strong>Grundegenskaper:</strong>
      </p>
      <Attribute
        caption="STY"
        characterAttrKey="str"
        req={character.str.req}
        mod={character.str.mod}
        value={character.str.value}
        setAttribute={setAttribute}
        cost={character.str.cost}
      />
      <Attribute
        caption="SMI"
        characterAttrKey="dex"
        req={character.dex.req}
        mod={character.dex.mod}
        value={character.dex.value}
        setAttribute={setAttribute}
        cost={character.dex.cost}
      />
      <Attribute
        caption="FYS"
        characterAttrKey="con"
        req={character.con.req}
        mod={character.con.mod}
        value={character.con.value}
        setAttribute={setAttribute}
        cost={character.con.cost}
      />
      <Attribute
        caption="INT"
        characterAttrKey="int"
        req={character.int.req}
        mod={character.int.mod}
        value={character.int.value}
        setAttribute={setAttribute}
        cost={character.int.cost}
      />
      <Attribute
        caption="PSY"
        characterAttrKey="wis"
        req={character.wis.req}
        mod={character.wis.mod}
        value={character.wis.value}
        setAttribute={setAttribute}
        cost={character.wis.cost}
      />
      <Attribute
        caption="KAR"
        characterAttrKey="cha"
        req={character.cha.req}
        mod={character.cha.mod}
        value={character.cha.value}
        setAttribute={setAttribute}
        cost={character.cha.cost}
      />
      <Attribute
        caption="STO"
        characterAttrKey="siz"
        max={character.siz.max}
        min={character.siz.min}
        normal={character.siz.normal}
        value={character.siz.value}
        setAttribute={setAttribute}
        cost={character.siz.cost}
      />

      <Race changeRace={selectRace} />
      <Class changeClass={selectClass} />
    </>
  );
}

export default Dod;
