import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import Start from "./pages/Start";
import FourOhFour from "./pages/FourOhFour";
import Dod from "./pages/dod/Dod";
import Dnd from "./pages/dnd/Dnd";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DefaultLayout />}>
          <Route index element={<Start />} />
          <Route path="dod" element={<Dod />} />
          <Route path="dnd" element={<Dnd />} />
          <Route path="*" element={<FourOhFour />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
