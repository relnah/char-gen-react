import { ChangeEvent, useEffect } from "react";
import { AttributeValue } from "../../pages/dod/Dod";

type Props = {
  caption: string;
  value: number;
  mod?: number;
  req?: number;
  max?: number;
  min?: number;
  normal?: number;
  characterAttrKey: string;
  setAttribute: Function;
  cost: number;
};

function Attribute(props: Props) {
  //Destruct to be able to use as dep in useEffect
  const { normal, setAttribute }: { normal?: number; setAttribute: Function } =
    props;
  useEffect(() => {
    setAttribute({ attr: "siz", value: normal });
  }, [normal, setAttribute]);

  function changeAttributeEventHandler(event: ChangeEvent<HTMLInputElement>) {
    const { value, valueAsNumber, maxLength } = event.target;

    const attrValue: AttributeValue = {
      attr: props.characterAttrKey.toLowerCase(),
      value: valueAsNumber,
    };
    // input type number does not check max length itself
    if (value.length <= maxLength) {
      props.setAttribute(attrValue);
    }
  }

  const lowReq: boolean = (props.req ?? 0) > props.value + (props.mod ?? 0);

  return (
    <>
      <div>
        <strong>{props.caption}</strong> {props.req ? `(${props.req})` : ""}
        {lowReq ? "LÅG" : ""}
        <br />
        Köp:
        <input
          value={props.value}
          maxLength={2}
          type="number"
          aria-label={props.caption}
          onChange={changeAttributeEventHandler}
          style={{ width: "3ch" }}
        />{" "}
        {props.normal
          ? `Normal: ${props.normal} (${props.min} / ${props.max}) `
          : `Mod: ${props.mod} `}
        Värde: {props.value + (props.mod ?? 0)} Kostnad: {props.cost}
      </div>
    </>
  );
}

export default Attribute;
