import { ChangeEvent } from "react";
import { classes } from "../../data/dod";
export type ClassType = {
  name: string;
  str?: number;
  dex?: number;
  con?: number;
  int?: number;
  wis?: number;
  cha?: number;
};

type Props = {
  changeClass: Function;
};

function Class(props: Props) {
  function changeClassEventHandler(event: ChangeEvent<HTMLSelectElement>) {
    props.changeClass(classes[event.target.selectedIndex - 1]);
  }

  return (
    <>
      <h2>Klass</h2>
      <select onChange={changeClassEventHandler}>
        <option value="none" selected disabled hidden></option>
        {classes.map((option, index) => (
          <option key={index} value={index}>
            {option.name}
          </option>
        ))}
      </select>
    </>
  );
}

export default Class;
