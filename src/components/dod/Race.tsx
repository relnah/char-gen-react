import { ChangeEvent } from "react";
import { races } from "../../data/dod";

export type SizeLimit = {
  normal: number;
  max: number;
  min: number;
};
export type RaceType = {
  name: string;
  str: number;
  dex: number;
  con: number;
  int: number;
  wis: number;
  cha: number;
  siz: SizeLimit;
  cost: number;
};

type Props = {
  changeRace: Function;
};

function Race(props: Props) {
  function changeRaceEventHandler(event: ChangeEvent<HTMLSelectElement>) {
    props.changeRace(races[event.target.selectedIndex - 1]);
  }

  return (
    <>
      <h2>Dod Ras</h2>
      <select onChange={changeRaceEventHandler}>
        <option value="none" selected disabled hidden></option>
        {races.map((option, index) => (
          <option key={index} value={index}>
            {option.name}
          </option>
        ))}
      </select>
    </>
  );
}

export default Race;
