import { ChangeEvent } from "react";

export type RaceType = {
  name: string;
  str: number;
};

type RaceProps = {
  race: RaceType;
  changeRace: Function;
};

const races: RaceType[] = [
  { name: "Elf", str: 0 },
  { name: "Dwarf", str: 2 },
];

function Race(props: RaceProps) {
  function changeRaceEvent(event: ChangeEvent<HTMLSelectElement>) {
    props.changeRace(races[event.target.selectedIndex]);
  }

  return (
    <>
      <h2>DnD Race</h2>
      <select onChange={changeRaceEvent}>
        {races.map((option: RaceType, index: number) => (
          <option key={index} value={index}>
            {option.name}
          </option>
        ))}
      </select>
    </>
  );
}

export default Race;
