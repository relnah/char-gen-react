FROM node:18-slim AS builder

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json'
COPY package.json ./
COPY package-lock.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build


FROM node:18-slim

WORKDIR /app

COPY --from=builder /app/build .

# install simple http server for serving static content
RUN npm -g install http-server

EXPOSE 8080
CMD [ "http-server", "./" ]